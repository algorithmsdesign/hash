/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.hash;

/**
 *
 * @author Pattrapon N
 */
public class hash {
    int M;
    Node[] list;

    public hash() { 
        this.M = 5;
        list = new Node[M];
    }

    public hash(int M) { 
        this.M = M;
        list = new Node[M];
    }

    class Node {
        int key;
        String val;
        Node next;

        Node(int key1, String val1) {
            key = key1;
            val = val1;
        }
    }

    private int hash(int key) { 
        int h = key % M;
        return h;
    }


    public String get(int key) {
      
        int h = hash(key);
        for (Node i = list[h]; i != null; i = i.next) {
            if (key == i.key) {
                return i.val;
            }
        }
        return null;
    }

    public void put(int key, String val) {
        int h = hash(key);
        Node head = list[h];

        while (head != null) {
            if ((head.key) == key) {
                head.val = val;
                return;
            }
            head = head.next;
        }
        head = list[h];
        Node node = new Node(key, val);
        node.next = head;
        list[h] = node;
    }

    public void remove(int key) {
        int h = hash(key);
        Node head = list[h];
        Node prev = null;

        while (head != null) {
            if (head.key == key) {
                break;
            }
            prev = head;
            head = head.next;
        }
        if (head == null) {
            return;
        }

        if (prev != null) {
            prev.next = head.next;
        } else {
            list[h] = head.next;
        }
    }

    public void getAll() {
        for (int i = 0; i < M; i++) {
            for (Node j = list[i]; j != null; j = j.next) {
                System.out.println("Index: "+i + " Key: " + j.key + " " +" Value: "+ j.val);
            }
        }
    }
}
